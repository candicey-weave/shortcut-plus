package com.gitlab.candicey.shortcutplus

data class Shortcut(
    val shorten: String,
    val command: List<String>,
) {
    companion object {
        fun parse(args: List<String>): Shortcut = Shortcut(args[0], args.slice(1 until args.size))
    }
}
