package com.gitlab.candicey.shortcutplus

import com.gitlab.candicey.zenithcore.config.Config
import com.gitlab.candicey.zenithcore.config.ConfigManager
import java.io.File

val configManager = ConfigManager(
    mutableMapOf(
        "shortcuts" to Config(File("~/.weave/ShortcutPlus/shortcuts.json"), ShortcutsConfig())
    )
)

val shortcutsConfig by configManager.delegateConfig<ShortcutsConfig>()

val shortcuts by lazy { shortcutsConfig.config.shortcuts }

data class ShortcutsConfig (
    val shortcuts: MutableList<Shortcut> = mutableListOf()
)