package com.gitlab.candicey.shortcutplus

import com.gitlab.candicey.zenithcore.extension.addChatMessage
import com.gitlab.candicey.zenithcore.extension.sendChatMessage
import com.gitlab.candicey.zenithcore.extension.toChatComponent
import com.gitlab.candicey.zenithloader.ZenithLoader
import com.gitlab.candicey.zenithloader.dependency.Dependencies.zenithCore
import com.gitlab.candicey.zenithloader.dependency.version
import net.weavemc.loader.api.ModInitializer
import net.weavemc.loader.api.command.Command
import net.weavemc.loader.api.command.CommandBus
import net.weavemc.loader.api.event.ChatSentEvent
import net.weavemc.loader.api.event.EventBus
import net.weavemc.loader.api.event.StartGameEvent

class ShortcutPlusMain : ModInitializer {
    override fun preInit() {
        ZenithLoader.loadDependencies(
            zenithCore version "@@ZENITH_CORE_VERSION@@"
        )

        EventBus.subscribe(StartGameEvent.Pre::class.java) {
            configManager.init()
        }

        EventBus.subscribe(ChatSentEvent::class.java) { event ->
            if (!event.message.startsWith("/")) {
                return@subscribe
            }

            val split = event.message.substring(1).split("\\s+".toRegex())
            if (split.isEmpty()) {
                return@subscribe
            }

            shortcuts
                .filter { it.shorten == split[0] }
                .forEach { shortcut ->
                    "/${shortcut.command.joinToString(" ")}"
                        .toChatComponent()
                        .sendChatMessage()

                    event.cancelled = true
                }
        }

        CommandBus.register(object : Command("shortcut") {
            override fun handle(args: Array<out String>) {
                if (args.isEmpty()) {
                    "§c§l[ShortcutPlus] §r§cPlease specify a shortcut name"
                        .toChatComponent()
                        .addChatMessage()
                    return
                }

                val option = args[0]
                val sliced by lazy { args.slice(1 until args.size) }

                fun saveConfig() {
                    shortcutsConfig.writeConfig()
                }

                when (option) {
                    "add" -> {
                        if (sliced.size < 2) {
                            "§c§l[ShortcutPlus] §r§cPlease specify a shortcut name and command"
                                .toChatComponent()
                                .addChatMessage()
                            return
                        }

                        shortcuts += Shortcut.parse(sliced)

                        saveConfig()
                    }

                    "remove" -> {
                        if (sliced.isEmpty()) {
                            "§c§l[ShortcutPlus] §r§cPlease specify a shortcut name"
                                .toChatComponent()
                                .addChatMessage()
                            return
                        }

                        shortcuts.removeIf { it.shorten == sliced[0] }

                        saveConfig()
                    }

                    "list" -> {
                        fun Shortcut.toChatComponent() = "§e§l${this.shorten} §r§7${this.command.joinToString(" ")}".toChatComponent()

                        "§e§l[ShortcutPlus] §r§7Shortcut list:"
                            .toChatComponent()
                            .addChatMessage()
                        shortcuts.forEach { it.toChatComponent().addChatMessage() }
                    }

                    "clear" -> {
                        shortcuts.clear()

                        saveConfig()
                    }

                    else -> {
                        "§c§l[ShortcutPlus] §r§cUnknown option: $option"
                            .toChatComponent()
                            .addChatMessage()
                        "§c§l[ShortcutPlus] §r§cAvailable options: add, remove, list, clear"
                            .toChatComponent()
                            .addChatMessage()
                    }
                }
            }
        })
    }
}